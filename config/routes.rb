Rails.application.routes.draw do

  get 'password_resets/new'

  get 'password_resets/edit'

#Index page
  root 'static_pages#home'

  get  '/help',    to: 'static_pages#help'
  get  '/about',   to: 'static_pages#about'
  get  '/contact', to: 'static_pages#contact'

#Sign-up page
  resources :users
  get  '/signup',  to: 'users#new'
  post '/signup',  to: 'users#create'

#Login page
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'

#User follower and following
  resources :users do
    member do
      get :following, :followers
    end
  end

#Account-activation
  resources :account_activations, only: [:edit]

#Password reset
  resources :password_resets,     only: [:new, :create, :edit, :update]

#Micro Post Create and Destroy
  resources :microposts,          only: [:create, :destroy]

#Follow - Un_follow relationship
  resources :relationships,       only: [:create, :destroy]

end
